import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
import { Employee } from '../model/employee';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  
  constructor(private http:HttpClient, private router: Router) { }

  getEmployees(){
    return this.http.get("http://localhost:5000/users")
  }

  getDataById(id:any){
    return this.http.get("http://localhost:5000/users/"+id)
  }

  postEmployee(employee:Employee){
    // console.log(employee,"employee data")
    return this.http.post("http://localhost:5000/users/add", employee)  
  }

  updateEmployee(id:any, employee:Employee){
    console.log(employee, "employee data")
    return this.http.put("http://localhost:5000/users/"+id, employee)
  }

  deleteEmployee(id:any){
    return this.http.delete("http://localhost:5000/users/"+id)
  }

}







