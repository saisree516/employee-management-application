export class Employee {
    constructor(
      public _id:any,
      public FirstName: String,
      public LastName: String,
      public Email: String,
      public Phoneno: Number,
      public Department: String,
      public Project: String,
      public Address: String
    ) {}
  }
  