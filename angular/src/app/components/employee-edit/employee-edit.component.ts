import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from 'src/app/model/employee';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css'],
})
export class EmployeeEditComponent implements OnInit {
  employees!: Employee;
  id: any;
  data: any;
  constructor(
    private employee: EmployeeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  form = new FormGroup({
    FirstName: new FormControl(''),
    LastName: new FormControl(''),
    Email: new FormControl(''),
    Phoneno: new FormControl(''),
    Department: new FormControl(''),
    Project: new FormControl(''),
    Address: new FormControl(''),
  });

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    console.log(this.id);
    this.getEmployeeData();
  }

  getEmployeeData() {
    this.employee.getDataById(this.id).subscribe((res) => {
      this.data = res;
      this.employees = this.data;
      console.log(this.employees);
      this.form = new FormGroup({
        FirstName: new FormControl(this.employees.FirstName),
        LastName: new FormControl(this.employees.LastName),
        Email: new FormControl(this.employees.Email),
        Phoneno: new FormControl(this.employees.Phoneno),
        Department: new FormControl(this.employees.Department),
        Project: new FormControl(this.employees.Project),
        Address: new FormControl(this.employees.Address),
      });
    });
  }

  updateEmployeeData() {
    this.employee.updateEmployee(this.id, this.form.value).subscribe(res => {
      this.data = res;
      console.log(this.data);
      // this.toastr.success("data updated successfully")
      this.router.navigateByUrl('/allemp');
    });
  }
}
