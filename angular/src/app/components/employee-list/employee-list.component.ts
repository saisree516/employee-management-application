import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees:any;
  constructor(private employeeService:EmployeeService) { }

  ngOnInit(): void {
    this.getEmployeesData()
  }

  getEmployeesData(){
    this.employeeService.getEmployees().subscribe(res=>{
      console.log(res)
      this.employees=res;
    })
  }

  deleteEmployeesData(id:any){
    // this.employees.splice(id-1,1)
    this.employeeService.deleteEmployee(id).subscribe(res=>{
      this.employees=res;
      this.ngOnInit()
    })
  }

}
