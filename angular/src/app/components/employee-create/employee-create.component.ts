import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {
  form!:FormGroup;
  submitted:boolean=false;
  data:any;

  constructor(private employeeService:EmployeeService,private formBuilder:FormBuilder,private router:Router) {    }
   createForm(){
     this.form=this.formBuilder.group({
       FirstName:['',[Validators.required, Validators.pattern('^[a-zA-Z].{8,}')]],
       LastName:['',[Validators.required, Validators.pattern('^[a-zA-Z].{8,}')]],
       Email:['',[Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
       Phoneno:['',[Validators.required, Validators.pattern('^[0-9]+$')]],
       Department:['',[Validators.required, Validators.pattern('^[a-zA-Z].{5,}')]],
       Project:['',[Validators.required, Validators.pattern('^[a-zA-Z].{8,}')]],
       Address:['',[Validators.required, Validators.pattern('^[a-zA-Z].{8,}')]]
     })
   }

  ngOnInit(): void {
    this.createForm();
  }

  get f(){
    return this.form.controls;
   }
   
  createEmployee(){
    this.submitted=true;
    if(this.form.invalid){
      return;
    }
    this.employeeService.postEmployee(this.form.value).subscribe((res: any)=>{
      this.data=res;
      // this.tostr.success("data saved successfully")
      this.router.navigateByUrl("/allemp")
    })
  }

}
