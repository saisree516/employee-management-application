import express from "express";
import { get_employeeBy_Id_handler, get_employee_details_handler } from "../path_handlers/get_api.js";
import { Post_Employee_details_handler } from "../path_handlers/post_api.js";
import { update_employee_handler } from "../path_handlers/update_api.js";
import { delete_employee_handler } from "../path_handlers/delete_api.js";

const router=express.Router();

// api call to retrive the employee details
router.get('/', get_employee_details_handler)

// api call to post the employee details
router.post('/add', Post_Employee_details_handler)

// api call to retrieve the employee details based on id
router.get('/:id', get_employeeBy_Id_handler);

// api call to update the employee project details based on department
router.put('/:id', update_employee_handler)

// api call to delete the employee details based on firstname
router.delete('/:id', delete_employee_handler)

export default router;