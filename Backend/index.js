// external dependencies
import express from "express";
import bodyparser from "body-parser";
import cors from "cors";

// internal dependencies
import Routes from './Routes/routes.js'

// Intializing app variable to the express application
const app=express();

// to support JSON En-coded bodies
app.use(bodyparser.json({extended:true}));

// to support urlencoded bodies
app.use(bodyparser.urlencoded({extended:true}))

// 
app.use(cors());

app.use(express.json())

app.use('/users', Routes);

// app.listen() function is used to bind and listen the connections on the specified host and port.
app.listen(5000, () => {
    console.log("listening to port 5000");
});
  