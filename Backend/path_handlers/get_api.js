import { EmployeeData } from "../dbutils/model.js";

/**
 * This function is used to retrive the employee details.
 * @param {*} request is used to get the employeedetails from the server
 * @param {*} response sends statuscode 200 if the data is retrieved successfully
 * else we get the error message
 */
const get_employee_details_handler = async (request, response) => { 
    try {
        const data = await EmployeeData.find();       
        response.status(200).json(data);
    } catch (error) {
        response.status(404).json({ message: error.message });
    }
}

/**
 * This function is used to retrive the employee details.
 * @param {*} req is used to get the employeedetails based on the id
 * @param {*} res sends statuscode 200 if the data is retrieved successfully
 * else we get the error message
 */
const get_employeeBy_Id_handler=async(request,response)=>{
    try {
        const data=await EmployeeData.findById(request.params.id);
            response.status(209).json(data);
    } catch (error) {
        response.status(404).json({message:error.message})     
    }
}

export { get_employee_details_handler,  get_employeeBy_Id_handler}