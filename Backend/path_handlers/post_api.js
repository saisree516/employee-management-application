// importing EmployeeData
import { response } from "express";
import { EmployeeData } from "../dbutils/model.js";

/**
 * This function is used to post the employee details.
 * @param {*} req by using this method we can post the employee details into the database 
 * @param {*} res sends statuscode 200 if the data is posted successfully
 * else we get the error message a
 */
const Post_Employee_details_handler = async(request, response) => {
  const data=request.body;
  const addEmployee=new EmployeeData(data);
  try{
    await addEmployee.save();
    response.status(200).json(addEmployee);
  }catch(error){
    response.status(409).json({message:error.message})
  }
}

  export { Post_Employee_details_handler };


 
