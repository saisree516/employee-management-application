import { EmployeeData } from "../dbutils/model.js"

/**
 * This function is used to delete the employee details.
 * @param {*} request this method is to delete the records based on the id
 * @param {*} response sends statuscode 200 if the data is deleted successfully
 * else we get the error message
 */
const delete_employee_handler=async( request, response) =>{
    try{
    await EmployeeData.deleteOne({_id:request.params.id})
    response.status(200).json("employee details deleted")
     } catch(error){
        console.log(error)
        response.status(409).json({ message: error.message });
    }
}

export { delete_employee_handler }

