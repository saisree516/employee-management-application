import { request } from "express";
import { EmployeeData } from "../dbutils/model.js"

/**
 * This function is used to update the employee details.
 * @param {*} req by using this method we can update the employee details based 
 * on the id into the database 
 * @param {*} res sends statuscode 200 if the data is updated successfully
 * else we get the error message
 */
const update_employee_handler=async( request, response) =>{
    let data=await EmployeeData.findById(request.params.id)
    data=request.body;
    const updateEmployee=new EmployeeData(data)
    try{
        await EmployeeData.updateOne({_id:request.params.id},updateEmployee);
        response.status(200).json(updateEmployee);
     } catch(error){
        console.log(error)
        response.status(409).json({message: error.message});
    }
}
export { update_employee_handler }

