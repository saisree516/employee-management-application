import { useState } from "react";
import {
  FormGroup,
  FormControl,
  InputLabel,
  Input,
  Button,
  makeStyles,
  Typography,
} from "@material-ui/core";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import { Alert } from '@mui/material'
import { addEmployee } from "../Service/api";
import { useHistory } from "react-router-dom";
import {
  _address_validator,
  _department_validator,
  _email_validator,
  _firstname_validator,
  _lastname_validator,
  _phoneNo_validator,
  _project_validator,
} from "./Validations";

const useStyles = makeStyles({
  container: {
    width: "40%",
    marginLeft: "30%",
    margin: "60px",
    "& > *": {
      marginTop: 20,
    },
  },
  background: {
    background: "linear-gradient(to right top, #FFCCFF, #ffffff)",
    height: "150vh",
    width: "100%",
    overFit: "auto",
  },
  button: {
    background: "#957DAD",
  },
  error:{
    color:"red"
  }
});

toast.configure()

const AddEmployee = () => {
  const [employee, setEmployee] = useState("");
  const [firstName, setFirstName] = useState("");
  const [firstNameErr, setFirstNameErr] = useState("");
  const [LastName, setLastName] = useState("");
  const [LastNameErr, setLastNameErr] = useState("");
  const [Email, setEmail] = useState("");
  const [EmailErr, setEmailErr] = useState("");
  const [Phoneno, setPhoneno] = useState("");
  const [PhonenoErr, setPhonenoErr] = useState("");
  const [Department, setDepartment] = useState("");
  const [DepartmentErr, setDepartmentErr] = useState("");
  const [Project, setProject] = useState("");
  const [ProjectErr, setProjectErr] = useState("");
  const [Address, setAddress] = useState("");
  const [AddressErr, setAddressErr] = useState("");

  const classes = useStyles();
  let history = useHistory();

  const addEmployeeDetails = async () => {
    const firstNameMessage = _firstname_validator(firstName)
    const lastNameMessage = _lastname_validator(LastName)
    const emailMessage=_email_validator(Email)
    const phonenoMessage=_phoneNo_validator(Phoneno)
    const departmentMessage=_department_validator(Department)
    const projectMessage=_project_validator(Project)
    const addressMessage=_address_validator(Address)
  
    if(firstNameMessage.length>0||lastNameMessage.length>0||emailMessage.length>0
      ||phonenoMessage.length>0||departmentMessage.length>0||projectMessage.length>0
      ||addressMessage.length>0){
      setFirstNameErr(_firstname_validator(firstName));
      setLastNameErr(_lastname_validator(LastName))
      setEmailErr(_email_validator(Email))
      setPhonenoErr(_phoneNo_validator(Phoneno))
      setDepartmentErr(_department_validator(Department))
      setProjectErr(_project_validator(Project))
      setAddressErr(_address_validator(Address))
      return 
    }
    await addEmployee(employee);
    toast.success("Employee details added successfully", {
      position: toast.POSITION.TOP_RIGHT, autoClose:10000, severity:"success"})
    history.push("./all");
  };

  const employeeNameVlidatorHandler = () => {
    const message = _firstname_validator(firstName);
    if (message) {
      setFirstNameErr(message);
    }
    console.log("firstName", firstName);
  };

  const employeeLastNameHandler = () => {
    const message = _lastname_validator(LastName);
    if (message) {
      setLastNameErr(message);
    }
    console.log("lastname", LastName);
  };

  const EmailHandler = () => {
    const message = _email_validator(Email);
    if (message) {
      setEmailErr(message);
    }
    console.log("email", Email);
  };

  const PhonenoHandler = () => {
    const message = _phoneNo_validator(Phoneno);
    if (message) {
      setPhonenoErr(message);
    }
    console.log("Phoneno", Phoneno);
  };

  const DepartmentHandler = () => {
    const message = _department_validator(Department);
    if (message) {
      setDepartmentErr(message);
    }
    console.log("Department", Department);
  };

  const ProjectHandler = () => {
    const message = _project_validator(Project);
    if (message) {
      setProjectErr(message);
    }
    console.log("Project", Project);
  };

  const AddressHandler = () => {
    const message = _address_validator(Address);
    if (message) {
      setAddressErr(message);
    }
    console.log("Address", Address);
  };

  return (
    <div className={classes.background}>
      <FormGroup className={classes.container}>
        <Typography variant="h6">Add Employee Details</Typography>
        <FormControl>
          <InputLabel htmlFor="my-input">First Name</InputLabel>
          <Input
            onChange={(e) => (
              setFirstName(e.target.value),
              setFirstNameErr(""),
              setEmployee({ ...employee, FirstName: e.target.value })
            )}
            onBlur={employeeNameVlidatorHandler}
            name="FirstName"
            value={employee.FirstName}
            id="my-input"
          />
          <div className={classes.error}>{firstNameErr}</div>
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Last Name</InputLabel>
          <Input
            onChange={(e) => (
              setLastName(e.target.value),
              setLastNameErr(""),
              setEmployee({ ...employee, LastName: e.target.value })
            )}
            onBlur={employeeLastNameHandler}
            name="LastName"
            value={employee.LastName}
            id="my-input"
          />
          <div className={classes.error}>{LastNameErr}</div>
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Email</InputLabel>
          <Input
            onChange={(e) => (
              setEmail(e.target.value),
              setEmailErr(""),
              setEmployee({ ...employee, Email: e.target.value })
            )}
            onBlur={EmailHandler}
            name="Email"
            value={employee.Email}
            id="my-input"
          />
          <div className={classes.error}>{EmailErr}</div>
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Phoneno</InputLabel>
          <Input
            onChange={(e) => (
              setPhoneno(e.target.value),
              setPhonenoErr(""),
              setEmployee({ ...employee, Phoneno: e.target.value })
            )}
            onBlur={PhonenoHandler}
            name="Phoneno"
            value={employee.Phoneno}
            id="my-input"
          />
          <div className={classes.error}>{PhonenoErr}</div>
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Department</InputLabel>
          <Input
            onChange={(e) => (
              setDepartment(e.target.value),
              setDepartmentErr(""),
              setEmployee({ ...employee, Department: e.target.value })
            )}
            onBlur={DepartmentHandler}
            name="Department"
            value={employee.Department}
            id="my-input"
          />
          <div className={classes.error}>{DepartmentErr}</div>
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Project</InputLabel>
          <Input
            onChange={(e) => (
              setProject(e.target.value),
              setProjectErr(""),
              setEmployee({ ...employee, Project: e.target.value })
            )}
            onBlur={ProjectHandler}
            name="Project"
            value={employee.Project}
            id="my-input"
          />
          <div className={classes.error}>{ProjectErr}</div>
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Address</InputLabel>
          <Input
            onChange={(e) => (
              setAddress(e.target.value),
              setAddressErr(""),
              setEmployee({ ...employee, Address: e.target.value })
            )}
            onBlur={AddressHandler}
            name="Address"
            value={employee.Address}
            id="my-input"
          />
          <div className={classes.error}>{AddressErr}</div>
        </FormControl>
        <FormControl>
          <Button
            variant="contained"
            className={classes.button}
            onClick={() => addEmployeeDetails()}
          >
            Add
          </Button>
        </FormControl>
      </FormGroup>
    </div>

        
      
  );
};

export default AddEmployee;
