import { Navbar, Container, Nav } from "react-bootstrap";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  navColor: {
    backgroundColor: "#957DAD",
  },
});

function NavBar() {
  const classes = useStyles()

  return (
    <>
      <Navbar
        CollapseOnSelected
        fixed="top"
        expand="sm"
        variant="light"
        className={classes.navColor}
      >
        <Container>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav>
            <img src=
            "https://subscribers-prod.s3.amazonaws.com/uploads/setting/modal_image/4368/icon.png"
           alt="" style={{height:50, width:50, marginTop:5,}}/>
            <Nav.Link href="/" style={{marginTop:7, color:"white"}}>Home</Nav.Link>
              <Nav.Link href="/add" style={{marginTop:7, color:"white"}}>Add Employee Details</Nav.Link>
              <Nav.Link href="/all" style={{marginTop:7, color:"white"}}>AllEmployeesList</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default NavBar;
