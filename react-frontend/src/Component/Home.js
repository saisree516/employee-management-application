import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles({
  image: {
    height:"auto",
    width:"100%"
  },
});

const HomePage = () => {
  const classes = useStyle();
  return (
    <>
      <div style={{marginTop:"50px",height:"100vh",width:"100%"}}>
        <img
          src="https://images.pond5.com/employee-management-animated-word-cloud-072404672_prevstill.jpeg"
          alt=""
          className={classes.image}
        />
      </div>
    </>
  );
};

export default HomePage;
