import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DeleteIcon from "@material-ui/icons/Delete";
import { FormHelperText, makeStyles } from '@material-ui/core';
// import { Direction } from 'react-toastify/dist/utils';
import EditIcon from "@material-ui/icons/Edit";
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
deleteicon: {
    color: "red",
    // marginLeft:80,
    // marginBottom:80,
    marginLeft:90,
    fontSize:30,
},
  yes:{
      color:"green"
  },
  no:{
      color:"red",
     
   
  }
});

export default function DeleteDialog({deleteEmployee}) {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  
  const handleClickOpen = () => {
    setOpen(true);
  };
  
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
        
      <DeleteIcon  className={classes.deleteicon} onClick={handleClickOpen} > </DeleteIcon>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to Delete?
          </DialogContentText>
        </DialogContent>
        <div className={classes.divv}>
        <DialogActions >
            <div>
          <Button onClick={()=>{handleClose();deleteEmployee()}} 
          className={classes.yes}>Yes</Button></div>
          <div>
          <Button onClick={handleClose} className={classes.no}>
            No
          </Button>
          </div>
        </DialogActions>
        </div>
      </Dialog>
    </div>
  );
}