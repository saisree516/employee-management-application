import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import EditIcon from "@material-ui/icons/Edit";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { useState } from "react";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import { deleteEmployee, getEmployees } from "../Service/api";
import DeleteDialog from "./deleteDailog";

const useStyles = makeStyles({
  root: {
    width: "300px",
    height: "400px",
    margin: "20px",
    backgroundColor: "#EFF5F5",
  },
  bg: {
    background: "linear-gradient(to right top, #FFCCFF, #ffffff)",
    padding: 20,
    height:"200vh",
  },
  cardcontent: {
    color: "black",
  },
  icon_rtl: {
    marginRight: 20,
    background:
      "url(https://static.thenounproject.com/png/101791-200.png) no-repeat right",
    backgroundSize: 20,
    backgroundColor: "white",
    width: 400,
    height: 30,
    paddingLeft: 10,
  },
  container: {
    display: "flex",
    flexDirection: "row",
    flexwrap: "wrap",
    overflowX: "auto",
    overflowY: "auto",
    // display: "flex",
    justifyContent: "center",
    // flexDirection: "row",
    flexWrap: "wrap",
  },
  searchContainer: {
    margin: 70,
    display: "flex",
    justifyContent: "center",
    marginBottom: "10px",
  },
  icon_rtl: {
    marginRight: 20,
    background: "url(https://static.thenounproject.com/png/101791-200.png) no-repeat right",
    backgroundSize: 20,
    backgroundColor: "white",
    width: 400,
  },
  editicon: {
    color: "green",
    fontSize:30,
    margin:5,
    // marginTop:70,
    marginLeft:90
  },
  card: {
    backgroundColor: "#D291BC",
  },
  notfound:{
    fontFamily:"Cursive",
    fontSize:"50px",
    textAlign:"center",
    paddingTop:100,
    fontStyle:"bold"
  }
});

export default function AllEmployees() {
  const [employees, setEmployeees] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  const classes = useStyles();

  useEffect(() => {
    getAllEmployees();
  }, []);

  const deleteEmployeeData = async (id) => {
    await deleteEmployee(id);
    getAllEmployees();
  };

  const getAllEmployees = async () => {
    let response = await getEmployees();
    setEmployeees(response.data);
  };

  const onChangeSearchInput = (event) => {
    setSearchInput(event.target.value);
  };

  const searchResults = employees.filter((eachEmployee) =>
    eachEmployee.FirstName
      ? eachEmployee.FirstName.toLowerCase().includes(searchInput)
      : " "
  );
  console.log(searchResults);

  return (
    <div className={classes.bg}>
      <div className={classes.searchContainer}>
        <input
          className={classes.icon_rtl}
          placeholder="Search for Employee"
          type="search"
          onChange={onChangeSearchInput}
          value={searchInput}
        />
      </div>
      {searchResults.length === 0 ? <div>
     <h5 className={classes.notfound}>Employee Name Not Found...!</h5>
         </div>:
      <div className={classes.container}>
        {searchResults.map((employee) => (
          <Card className={classes.root}>
            <AppBar position="static" className={classes.card}>
              <Toolbar>
                {employee.FirstName} Profile
                <div>
                  <Link to={`/edit/${employee._id}`}>
                    <EditIcon className={classes.editicon} />
                  </Link>
                   <DeleteDialog deleteEmployee={()=>deleteEmployeeData(employee._id)}/>
                </div>
              </Toolbar>
            </AppBar>
            <CardContent className={classes.cardcontent}>
              <Typography variant="body2" component="P">
                First Name :{employee.FirstName}
              </Typography>
              <br></br>
              <Typography variant="body2" component="P">
                Last Name :{employee.LastName}
              </Typography>
              <br></br>
              <Typography variant="body2" component="p">
                Email :{employee.Email}
              </Typography>
              <br></br>
              <Typography variant="body2" component="p">
                Phoneno : {employee.Phoneno}
              </Typography>
              <br></br>
              <Typography variant="body2" component="p">
                Department :{employee.Department}
              </Typography>
              <br></br>
              <Typography variant="body2" component="p">
                Project :{employee.Project}
              </Typography>
              <br></br>
              <Typography variant="body2" component="p">
                Address :{employee.Address}
              </Typography>
            </CardContent>
          </Card>
        ))}
      </div>
}
    </div>
  );
}
