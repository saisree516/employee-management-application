import { useState, useEffect } from "react";
import {
  FormGroup,
  FormControl,
  InputLabel,
  Input,
  Button,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import { getEmployees, editEmployee } from "../Service/api";

const initialValue = {
  FirstName: "",
  LastName: "",
  Email: "",
  Phoneno: "",
  Department: "",
  Project: "",
  Address: "",
};

const useStyles = makeStyles({
  container: {
    width: "40%",
    marginLeft: "30%",
    margin: "60px",
    "& > *": {
      marginTop: 20,
    },
  },
  background: {
    background: "linear-gradient(to right top, #FFCCFF, #ffffff)",
    height: "150vh",
    width: "100vw",
  },
  button: {
    backgroundColor: "#957DAD",
  },
});

const EditEmployeeDetails = () => {
  const [employee, setEmployee] = useState(initialValue);
  const { FirstName, LastName, Email, Phoneno, Department, Project, Address } =
    employee;
  const { id } = useParams();
  const classes = useStyles();
  let history = useHistory();

  useEffect(() => {
    loadEmployeeDetails();
  }, []);

  const loadEmployeeDetails = async () => {
    const response = await getEmployees(id);
    setEmployee(response.data);
  };

  const editEmployeeDetails = async () => {
    const response = await editEmployee(id, employee);
    console.log(response);
    history.push("/all");
  };

  return (
    <div className={classes.background}>
      <FormGroup className={classes.container}>
        <Typography variant="h6">Edit EmployeeDetails</Typography>
        <FormControl>
          <InputLabel htmlFor="my-input">FirstName</InputLabel>
          <Input
            onChange={(e) =>
              setEmployee({ ...employee, FirstName: e.target.value })
            }
            name="FirstName"
            value={FirstName}
            id="my-input"
            aria-describedby="my-helper-text"
          />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">LastName</InputLabel>
          <Input
            onChange={(e) =>
              setEmployee({ ...employee, LastName: e.target.value })
            }
            name="LastName"
            value={LastName}
            id="my-input"
            aria-describedby="my-helper-text"
          />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Email</InputLabel>
          <Input
            onChange={(e) =>
              setEmployee({ ...employee, Email: e.target.value })
            }
            name="Email"
            value={Email}
            id="my-input"
            aria-describedby="my-helper-text"
          />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Phoneno</InputLabel>
          <Input
            onChange={(e) =>
              setEmployee({ ...employee, Phoneno: e.target.value })
            }
            name="Phoneno"
            value={Phoneno}
            id="my-input"
            aria-describedby="my-helper-text"
          />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Department</InputLabel>
          <Input
            onChange={(e) =>
              setEmployee({ ...employee, Department: e.target.value })
            }
            name="Department"
            value={Department}
            id="my-input"
            aria-describedby="my-helper-text"
          />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Project</InputLabel>
          <Input
            onChange={(e) =>
              setEmployee({ ...employee, Project: e.target.value })
            }
            name="Project"
            value={Project}
            id="my-input"
            aria-describedby="my-helper-text"
          />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Address</InputLabel>
          <Input
            onChange={(e) =>
              setEmployee({ ...employee, Address: e.target.value })
            }
            name="Address"
            value={Address}
            id="my-input"
            aria-describedby="my-helper-text"
          />
        </FormControl>
        <FormControl>
          <Button
            variant="contained"
            className={classes.button}
            onClick={() => editEmployeeDetails()}
          >
            Edit
          </Button>
        </FormControl>
      </FormGroup>
    </div>
  );
};

export default EditEmployeeDetails;
