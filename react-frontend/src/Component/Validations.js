/**
 * Helper function to validate name string
 * checks if name is string, else returns "name should be of string type"
 * checks if name is too short, else returns "name should be at least 4 characters long and max of 25"
 * checks if name is of the correct format, else returns "name should not have at least 1 special character and at least 1 digit"
 * @param {} FirstName
 * @returns
 */
const _firstname_validator = (FirstName) => {
  if (FirstName === "") {
    return "FirstName field is mandatory";
  }
  if (typeof FirstName != "string") {
    return "FirstName should be of string type";
  }
  if (FirstName.length < 4 || FirstName.length > 15) {
    return "FirstName length should be between 4 and 25 characters long";
  }
  if (!FirstName.match(/^[a-zA-Z ]+$/)) {
    return "FirstName should contain Alphabets";
  }
  return ""
};

/**
 * Helper function to validate name string
 * checks if name is string, else returns "name should be of string type"
 * checks if name is too short, else returns "name should be at least 4 characters long and max of 25"
 * checks if name is of the correct format, else returns "name should not have at least 1 special character and at least 1 digit"
 * @param {} LastName
 * @returns
 */
const _lastname_validator = (LastName) => {
  if (LastName === "") {
    return "LastName field is mandatory";
  }
  if (typeof LastName != "string") {
    return "LastName should be of string type";
  }
  if (LastName.length < 4 || LastName.length > 15) {
    return "LastName length should be between 4 and 25 characters long";
  }
  if (!LastName.match(/^[a-zA-Z ]+$/)) {
    return "LastName should contain Alphabets";
  }
  return ""
};

/**
 * Helper function to validate email string
 * checks if email is string, else returns "email should be of string type"
 * checks if email is too short, else returns "email should be at least 6 characters long and max of 50"
 * checks if email is of the correct format, else returns "email should be of the correct format" 406
 * @param {} Email
 * @returns
 */
const _email_validator = (Email) => {
  // is it a string
  if(Email === ""){
    return "Email field is mandatory"
  }
  if (typeof Email != "string") {
    return "email should be of string type";
  }
  if (Email.length < 6) {
    return "email length should be at least 6 characters long";
  }
  if (Email.length > 50) {
    return "email length should be a max of 50";
  }
  if (
    !Email.match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  ) {
    return "email should be of the correct format";
  }

  return "";
};

/**
 * Helper function to validate phone number
 * checks if phone no is number, else returns "phone number should be of number type"
 * checks if phone no is too short, else returns "phone number should be at least 1 characters long and max of 10"
 * checks if phone no is of the correct format, else returns "phone number should be of the correct format" 406
 * @param {} Phoneno
 * @returns
 */
const _phoneNo_validator = (Phoneno) => {
  if (Phoneno === "") {
    return "phone number field is mandatory";
  }
  if (!Phoneno.match(/^(\+\d{1,3}[- ]?)?\d{10}$/)){
    return "phone number should be in correct format"
  }
};

/*
 * Helper function to validate security_question string
 * checks if _security_question_validator is string, else returns "security_question should be of string"
 * @param {} Department
 * @returns
 */
const _department_validator = (Department) => {
  if (typeof Department != "string") {
    return "Department field is mandatory";
  }
  if (Department === "") {
    return "Department field is mandatory";
  }
  if (!Department.match(/^[a-zA-Z ]+$/)) {
    return "Department should contain only Alphabets";
  }
  return "";
};

/*
 * Helper function to validate security_answer string
 * checks if _security_answer_validator is string, else returns "security_answer should be of string"
 * @param {} Project
 * @returns
 */
const _project_validator = (Project) => {
  if (typeof Project != "string") {
    return "Project should be of string type";
  }
  if (Project === "") {
    return "Project field is mandatory";
  }
  if (!Project.match(/^[a-zA-Z ]+$/)) {
    return "Project should contain Alphabets";
  }
  return "";
};

/*
 * Helper function to validate security_answer string
 * checks if _security_answer_validator is string, else returns "security_answer should be of string"
 * @param {} Address
 * @returns
 */
const _address_validator = (Address) => {
  if (typeof Address != "string") {
    return "Address should be of string type";
  }
  if (Address === "") {
    return "Address field is mandatory";
  }
  if (!Address.match(/^[a-zA-Z ]+$/)) {
    return "Address should contain Alphabets";
  }
  return "";
};

export {
  _firstname_validator,
  _lastname_validator,
  _phoneNo_validator,
  _email_validator,
  _department_validator,
  _project_validator,
  _address_validator,
};
