import axios from 'axios';

const Url = 'http://localhost:5000/users';

export const getEmployees = async (id) => {
    id = id || '';
    return await axios.get(`${Url}/${id}`);
}

export const addEmployee = async (employee) => {
    return await axios.post(`${Url}/add`, employee);
}

export const deleteEmployee = async (id) => {
    return await axios.delete(`${Url}/${id}`);
}

export const editEmployee = async (id, employee) => {
    return await axios.put(`${Url}/${id}`, employee)
}