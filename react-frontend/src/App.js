import NavBar from './Component/Navbar';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AddEmployee from './Component/AddEmployee';
import AllEmployees from './Component/AllEmployeesList';
import  EditEmployeeDetails  from './Component/EditEmployee';
import HomePage from './Component/Home';

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Switch>
        <Route exact path="/" component={HomePage}/>
        <Route exact path="/all" component={AllEmployees} />
        <Route exact path="/add" component={AddEmployee} />
        <Route exact path="/edit/:id" component={EditEmployeeDetails} />
      </Switch>
    </BrowserRouter>

  );
}

export default App;
